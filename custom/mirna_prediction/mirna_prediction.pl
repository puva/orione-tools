#!/usr/bin/perl

# Version 1.0
#
# Identification of miRNA using NGS data
# This script integrates SeqTrimMap and miRDeep2
# Creation: December 2012
# Last update: January 2013
#
# Authors: Paolo Uva

use warnings;
use strict;
use Getopt::Std;
use File::Basename;

# Usage message
my $usage="$0 -r READSfile -g GENOMEfile -m MATURE -p PRECURSOR_REFfile -s SPECIES -d DIR -o OUTFILE -l LOGFILE\n";
die $usage if ( @ARGV < 16 );

# Processing input files
my %options;
getopts("r:g:m:p:s:d:o:l:",\%options);

my $reads	= $options {'r'};
my $genome	= $options {'g'};
my $mature	= $options {'m'};	
my $precursor	= $options {'p'};
my $species	= $options {'s'};
my $dir		= $options {'d'};
my $outfile	= $options {'o'};
my $logfile	= $options {'l'};             

# Define additonal variables
my $genome_ind = fileparse($genome, qr/\.[^.]*/);
my $basename = fileparse($reads, qr/\.[^.]*/);
my $command;

print "Started\n";

# Create genome index 
$command = "bowtie-build $genome $genome_ind";
if(! -e "$genome_ind.1.ebwt"){
  print "Running $command\n";
  system($command) == 0 || die "Command $command failed: $!\n";
}else{
  print "Index already exists -- skipping\n";
}

# Run SeqTriMap
$command = "SeqTrimMap -m 5 -b -S -i -v 0 -p 4 -l 18 -x 25 -o $basename $reads $genome_ind";
print "Running $command\n";
system($command) == 0 || die "Command $command failed: $!\n";

# Keep mapped reads only
# To Do: Replace by samtools
$command = "awk '\$3!=\"*\" {print \$0}' $basename.sam > $basename"."_mapped.sam";
print "Running $command\n";
system($command) == 0 || die "Command $command failed: $!\n";

# SAM converter
# This step uses a modified version of bwa_sam_converter.pl in the original miRDeep2 package
# which is able to manage reads present multiple times in different rows of the SAM file
$command = "bwa_sam_converter_v2.pl -c -i $basename"."_mapped.sam -o $basename"."_collapsed.fa -a $basename.arf";
print "Running $command\n";
system($command) == 0 || die "Command $command failed: $!\n";
print "Finished sam converter\n";


# Create files with mature sequences
$command = "split_mirbase.pl -i $mature -s $species -c mature_$species.fasta -o mature_not_in_$species.fasta";
print "Creating mature sequences\n";
system($command) == 0 || die "Command $command failed: $!\n";

# Create files with precursor sequences (for selected and other species)
$command = "split_mirbase.pl -i $precursor -s $species -c precursors_$species.fasta -o not_in_$species.fasta";
print "Creating precursor sequences\n";
system($command) == 0 || die "Command $command failed: $!\n";


# Run miRDeep2
$command = "miRDeep2.pl $basename"."_collapsed.fa $genome $basename.arf mature_$species.fasta mature_not_in_$species.fasta precursors_$species.fasta -P -t $species -v -r $basename"."_mirdeep_out 2> $basename"."_mirdeep.log";

print "Running $command\n";
system($command) == 0 || die "Command $command failed: $!\n";
print "Finished identification\n";

# Move all the output files in folder $dir
# It seems that files cannot be in a subfolder (see also rgFastQC.xml)
system("mv pdfs_* $dir") == 0 || die "Command failed: $!\n";

# Rename files by replacing the : by _
system("rename \'s/:/_/\' $dir/*pdf") == 0 || die "Command failed: $!\n";

# As images are not in the original subfolder, we need to
# replace the links in the original HTML file
opendir(DIR,".");
my @files=grep /result_.*html/, readdir(DIR);
closedir(DIR);
my $report_file = shift @files;

my @tooltips;
my $counter = 0;
print "Rewriting HTML file\n";
open(R, "$report_file") || die "Error reading file $report_file: $!\n";
open(W, ">$outfile") || die "Error writing file $outfile: $!\n";
while(<R>){
  # Remove pdfs_ pre-link
  s/pdfs[^\/]*\///g;

  # Replace : by _ in file name
  s/(href=\".*):(.*\.pdf)/$1_$2/;

  # Remove tooltips and append to the end of the page
  if(/<span>.*<\/span>/){
	# Replace text by progressive ID
	$counter++;
	s/<span>(.*)<\/span>/ \[$counter\]/;
	push(@tooltips,$1);
  }
  if(/<\/body>/){
	print W "<h2>Notes</h2>";
	foreach my $i (0..$#tooltips) {
	  print W "[".($i + 1)."] $tooltips[$i]<br>\n";
	}
  }
  print W;
}
close(R);
close (W);

# Combine all the log files (SeqTrimMap and miRDeep2.pl) in $log_file
$command="cat $basename.bowtie_stats $basename"."_mirdeep.log > $logfile";
system($command) == 0 || die "Command $command failed: $!\n";

print "Finished\n";
