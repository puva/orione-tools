#!/usr/bin/perl

# Split fasta files from miRBase 
# in two different files based on -s parameter
# Author: Paolo Uva
# Date: March 2013

use warnings;
use strict;
use Getopt::Std;


my $usage="$0 -i mature.fa -s hsa -c outfile_for_current_species -o outfile_for_other_species\n";
die $usage if ( @ARGV < 8 );

my %options;
getopts("i:s:o:c:",\%options);


# Processing input files
my $input_file  	= $options {'i'};
my $species     	= $options {'s'};
my $outfile_others	= $options {'o'};
my $outfile_current	= $options {'c'};

my $is_current_sp = 0;


open (CUR_SPECIES, ">$outfile_current") or die "Cannot open file $outfile_current: $!\n";

open (OTHER_SPECIES, ">$outfile_others") or die "Cannot open file $outfile_others: $!\n";

open(IN, $input_file) or die "Cannot open file $input_file: $!\n";
while(<IN>){

  $_ =~ s/^>(\S*).*$/>$1/;
  $is_current_sp = (/>$species-/) ? 1 : 0 if (/^>/);
  if ($is_current_sp){
    print CUR_SPECIES;
  }else{
    print OTHER_SPECIES;
  }
}

close(IN);
close(CUR_SPECIES);
close(OTHER_SPECIES);
