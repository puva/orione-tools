<tool id="muscle" name="MUSCLE" version="0.0.11">
  <description>multiple aligner</description>
  <requirements>
    <requirement type="package" version="3.8.31">muscle</requirement>
    <requirement type="set_environment">FASTA_MAX_SIZE</requirement>
  </requirements>
  <version_command>muscle -version</version_command>
  <command>
    FASTA_MAX_SIZE=\${FASTA_MAX_SIZE:-100000};
    if [ `stat -c "%s" $multifasta` -gt \$FASTA_MAX_SIZE ]; then echo "Multi-FASTA file is bigger than the configured limit for MUSCLE (\$FASTA_MAX_SIZE bytes)." >&amp;2; exit 1; fi;
    muscle -quiet -in $multifasta -out $output -log $log
    #if str($maxiters)
      -maxiters $maxiters
    #end if
    #if $diags
      -diags
    #end if
  </command>
  <inputs>
    <param name="multifasta" type="data" format="fasta" label="Multi-FASTA file" />
    <param name="maxiters" type="integer" value="2" min="1" max="16" optional="true" label="Maximum number of iterations to run (-maxiters)" help="Set to 2 for large datasets where refinement (iterations 3+) is too slow." />
    <param name="diags" type="boolean" label="Diagonal optimization (-diags)" help="Speed up the algorithm, especially for closely related sequences, at the expense of some reduction in accuracy" />
  </inputs>
  <outputs>
    <data name="output" format="fasta" label="${tool.name} on ${on_string}: aligned sequences"/>
    <data name="log" format="txt" label="${tool.name} on ${on_string}: log"/>
  </outputs>
  <tests>
  </tests>
  <help>
**What it does**

This tool performs multiple sequence alignment using MUSCLE.

**License and citation**

This Galaxy tool is Copyright © 2013-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `MUSCLE`_.

.. _MUSCLE: http://drive5.com/muscle/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Edgar2004|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Edgar2004| replace:: Edgar, R.C. (2004) MUSCLE: multiple sequence alignment with high accuracy and high throughput. *Nucleic Acids Res.* 32(5), 1792-1797
.. _Edgar2004: http://nar.oxfordjournals.org/content/32/5/1792
  </help>
  <citations>
    <citation type="doi">10.1093/nar/gkh340</citation>
  </citations>
</tool>
