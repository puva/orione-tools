#!/usr/bin/perl
# Copyright © 2013-2014 CRS4 Srl. http://www.crs4.it/
# Created by:
# Paolo Uva <paolo.uva@crs4.it>
# Nicola Soranzo <nicola.soranzo@crs4.it>
#
# Released under the MIT license http://opensource.org/licenses/MIT .

# Split a FASTQ file by lane

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use IO::Zlib;

my @lanes, my $prefix = '';
GetOptions ("lanes=s" => \@lanes, "prefix=s" => \$prefix);
@lanes = split(/,/, join(',', @lanes));
@lanes = (1..8) if ! @lanes;

usage() if scalar(@ARGV) != 1;
my $infile = $ARGV[0];

# Check whether the file exists
die "File $infile does not exist!\n" if ! -e $infile;

if (! $prefix) {
  basename($infile) =~ m/^(.*)\.(.*)$/ or die 'File name not recognized!';
  $prefix = $1;
}

if ($infile =~ m/\.gz$/) {
  tie *IN, 'IO::Zlib', $infile, 'rb' or die "Cannot open gzipped file $infile: $!";
} else {
  open(IN, $infile) or die "Cannot open $infile: $!";
}

my %fh;
foreach my $lane (@lanes) {
  open($fh{$lane}, '>', "${prefix}_L${lane}.fastq") or die "Cannot open file: $!";
}
while (my $fqEntry = <IN>) {
  $fqEntry =~ m/@[^:]+:[0-9]+:[^:]+:([1-8]):[0-9]+:[0-9]+:[0-9]+/ or $fqEntry =~ m/@[^:]+:([0-9]):[0-9]+:[0-9]+:[0-9]+#/ or die 'Lane number not found in FASTQ header';
  my $lane = $1;
  $fqEntry = $fqEntry . <IN> for (1..3); # each entry is 4 lines total. DON'T USE '.=', seems to be buggy for gzipped files
  print {$fh{$lane}} $fqEntry if exists $fh{$lane};
}
close(IN);
close($fh{$_}) for keys(%fh);


sub usage {
  print "Given one FASTQ file, split reads into multiple files by lane. Also accepts gz compressed files.\nUsage:\n   $0 file1 --lanes 1,3,5 --prefix output\n";
  exit;
}
