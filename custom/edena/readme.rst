Edena wrapper
=============

Configuration
-------------

edena_ovl_wrapper tool may be configured to use more than one CPU core by selecting an appropriate destination for this tool in Galaxy job_conf.xml file (see https://wiki.galaxyproject.org/Admin/Config/Jobs and https://wiki.galaxyproject.org/Admin/Config/Performance/Cluster ).

If you are using Galaxy release_2013.11.04 or later, this tool will automatically use the number of CPU cores allocated by the job runner according to the configuration of the destination selected for this tool.

If instead you are using an older Galaxy release, you should also add a line

  GALAXY_SLOTS=N; export GALAXY_SLOTS

(where N is the number of CPU cores allocated by the job runner for this tool) to the file

  <tool_dependencies_dir>/edena/3.131028/crs4/edena/<hash_string>/env.sh

Version history
---------------

- Unreleased: Do not discard stderr since it contains useful information in case of error, so use <stdio> to determine job failure. Update Orione citation.
- Release 2: Support Edena v. 3.131028 (new <version_command>, official overlapping log file, covStats output file removed, -lph and -sph options instead of -peHorizon). Use $GALAXY_SLOTS instead of $EDENA_SITE_OPTIONS. Directly call edena, remove edena_ovl_wrapper.py and edena_ass_wrapper.py . Discard stderr instead of redirecting to stdout. Do not redirect stdout to logfile. Add readme.rst .
- Release 1: Simplify passing repeated params to Python script. Add more info to help sections.
- Release 0: Initial release in the Tool Shed.

Development
-----------

Development is hosted at https://bitbucket.org/crs4/orione-tools . Contributions and bug reports are very welcome!
