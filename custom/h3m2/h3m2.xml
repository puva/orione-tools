<tool id="h3m2" name="RoH with H3M2" version="1.0">
  <description></description>
  <requirements>
    <requirement type="package" version="0.1.18">samtools</requirement>
    <requirement type="package" version="3.0.1">R</requirement>
    <requirement type="package" version="0.1">H3M2</requirement>
  </requirements>
  <command>
## Creates symlink to input BAM 
ln -s ${input} input.bam &amp;&amp;
ln -s ${input.metadata.bam_index} input.bam.bai &amp;&amp;

## Run H3M2
\$H3M2_PATH/H3M2BamParsing.sh \$H3M2_PATH . input.bam . project sample /SHARE/USERFS/els7/users/biobank/genome/hg19/srma_index/hg19full/hg19full.fa \$H3M2_PATH/SNP1000GP.HGb37_Exome.mod.bed &gt; ${log} 2&gt;&amp;1 &amp;&amp;
\$H3M2_PATH/H3M2Analyze.sh \$H3M2_PATH . project sample \$H3M2_PATH/SNP1000GP.HGb37_Exome.mod.bed $dnorm $p1 $p2 5 &gt;&gt; ${log} 2&gt;&amp;1 &amp;&amp;

## Move output files
mv project/sample/Results/*pdf $out_pdf &amp;&amp;
mv project/sample/Results/*bed $out_bed
  </command>
  <inputs>
    <param name="input" type="data" format="bam" label="Input BAM (hg19 only)" />
    <param name="dnorm" type="select" label="DNorm" help="Modulates the effect of genomic distance on the transition probabilities">
        <option value="100000" selected="true">100000</option>
        <option value="10000">10000</option>
        <option value="1000">1000</option>
    </param>
    <param name="p1" type="float" value="0.1" label="P1" help="Probability of moving from state 1 to state 2 in HMM" />
    <param name="p2" type="float" value="0.1" label="P2" help="Probability of moving from state 2 to state 1 in HMM" />
  </inputs>
  <outputs>
    <data name="out_pdf" format="pdf" label="${tool.name} on ${on_string}: pdf" />
    <data name="out_bed" format="tabular" label="${tool.name} on ${on_string}: tabular" />
    <data name="log" format="txt" label="${tool.name} on ${on_string}: log" />
  </outputs>
  <help>
**What it does**

This tool computes runs of homozygosity using H3M2 software.

**Output format**

The output file in tabular format contains 5 columns::

   -Chromosome
   -Start
   -End
   -Probability of autozygosity
   -Number of SNPs within the Run of Homozygosity

**License and citation**

This Galaxy tool is Copyright © 2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `H3M2`_.

.. _H3M2: http://sourceforge.net/projects/h3m2/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Magi2014|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Magi2014| replace:: Magi, A., Tattini, L., Palombo, F., Benelli, M., Gialluisi, A., Giusti, B., Abbate, R., Seri, M., Gensini, G.F., Romeo, G., Pippucci, T. (2014) H3M2: detection of runs of homozygosity from whole-exome sequencing data. *Bioinformatics* 30(20), 2852-2859
.. _Magi2014: http://bioinformatics.oxfordjournals.org/content/30/20/2852
  </help>
</tool>
