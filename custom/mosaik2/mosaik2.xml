<?xml version="1.0"?>
<tool id="mosaik2" name="Map with MOSAIK v. 2" version="2.2.28.0">
  <description/>
  <requirements>
    <requirement type="package" version="2.2.28">mosaik</requirement>
    <requirement type="package" version="0.1.19">samtools</requirement>
  </requirements>
  <version_command>MosaikJump | sed -e 's/\x1b\[[[:digit:]]\{1,2\}\(;[[:digit:]]\{1,2\}\)\{0,1\}m//g' | sed -n 's/MosaikJump \([[:digit:].]\{1,\}\).*/\1/p'</version_command>
  <command>
    MosaikBuild -fr
    #if $genomeSource.refGenomeSource == 'indexed'
      ${genomeSource.indexReference.fields.path}
    #else
      ${genomeSource.historyReference}
    #end if
    -oa mosaik_ref_file;
    MosaikBuild -q $reads
    #if $paired.kind == 'single'
      #set $ls_string = ''
    #else
      -q2 ${paired.reads2}
      -mfl ${paired.mfl}
      #set $ls_string = '-ls %s' % $paired.ls
    #end if
    -st $st -out mosaik_reads_file;
    MosaikAligner -p \${GALAXY_SLOTS:-4} -ia mosaik_ref_file -in mosaik_reads_file -out mosaik_aligned_file -annpe \$MOSAIK_NETWORK_DIR/2.1.78.pe.ann -annse \$MOSAIK_NETWORK_DIR/2.1.78.se.ann $ls_string
    #if str($mm)
      -mm $mm
    #end if
    #if str($act)
      -act $act
    #end if
    #if str($bw)
      -bw $bw
    #end if
    #if str($mhp)
      -mhp $mhp
    #end if
    -hs 15 &amp;&amp;
    #if str($outFormat) == 'bam'
      samtools sort -o mosaik_aligned_file.bam out_prefix &gt; $output
    #else
      samtools view mosaik_aligned_file.bam -o $output
    #end if
  </command>
  <inputs>
    <conditional name="genomeSource">
      <param name="refGenomeSource" type="select" label="Will you select a reference genome from your history or use a built-in index?">
        <option value="indexed">Use a built-in index</option>
        <option value="history">Use one from the history</option>
      </param>
      <when value="indexed">
        <param name="indexReference" type="select" label="Select a reference genome">
          <options from_data_table="mosaik_indexes">
            <filter type="sort_by" column="2" />
            <validator type="no_options" message="No indexes are available" />
          </options>
        </param>
      </when>
      <when value="history">
        <param format="fasta" name="historyReference" type="data" metadata_name="dbkey" label="Select a reference from history" />
      </when>
    </conditional>
    <param name="st" type="select" label="Sequencing technology used">
      <option value="454">454</option>
      <option value="illumina">Illumina</option>
      <option value="solid">Solid</option>
      <option value="sanger">Sanger</option>
      <option value="helicos">Helicos</option>
    </param>
    <param format="fastq" name="reads" type="data" label="FASTQ reads file" />
    <conditional name="paired">
      <param name="kind" type="select" label="Is this library mate-paired?">
        <option value="single">Single-end</option>
        <option value="paired">Paired-end</option>
      </param>
      <when value="single" />
      <when value="paired">
        <param format="fastq" name="reads2" type="data" label="FASTQ 2nd mate" />
        <param name="mfl" type="integer" value="200" label="Median fragment length" />
        <param name="ls" type="integer" min="0" value="50" label="Local alignment search radius to rescue mates" help="A large value slows down performances" />
      </when>
    </conditional>
    <param name="mm" type="integer" value="6" optional="true" label="Number of mismatches allowed per sequence" />
    <param name="act" type="integer" value="35" optional="true" label="Alignment candidate threshold" help="Determines which hash regions will be aligned with Smith-Waterman" />
    <param name="bw" type="integer" value="9" optional="true" label="Smith-Waterman band width" />
    <param name="mhp" type="integer" value="100" optional="true" label="Maximum number of positions stored per seed" help="Number of places in the reference the aligner will try to place a particular hash" />
    <param name="outFormat" type="select" label="Output format">
      <option value="sam">SAM</option>
      <option value="bam">BAM</option>
    </param>
  </inputs>
  <outputs>
    <data format="sam" name="output">
      <change_format>
        <when input="outFormat" value="bam" format="bam" />
      </change_format>
      <actions>
        <conditional name="genomeSource.refGenomeSource">
          <when value="indexed">
            <action type="metadata" name="dbkey">
              <option type="from_data_table" name="mosaik_indexes" column="1">
                <filter type="param_value" column="0" value="#" compare="startswith" keep="False" />
                <filter type="param_value" ref="genomeSource.indexReference" column="0" />
              </option>
            </action>
          </when>
          <when value="history">
            <action type="metadata" name="dbkey">
              <option type="from_param" name="genomeSource.historyReference" param_attribute="dbkey" />
            </action>
          </when>
        </conditional>
      </actions>
   </data>
  </outputs>
  <tests>
    <test>
      <param name="refGenomeSource" value="history" />
      <param name="historyReference" ftype="fasta" value="mosaik_test_ref.fasta" />
      <param name="st" value="454" />
      <param name="reads" ftype="fastq" value="mosaik_test_input.fastq" />
      <param name="kind" value="single" />
      <param name="mm" value="6" />
      <param name="act" value="35" />
      <param name="bw" value="9" />
      <param name="mhp" value="100" />
      <param name="outFormat" value="sam" />
      <output name="output" file="mosaik_test_out.sam" compare="sim_size" delta="0" />
    </test>
  </tests>
  <help>
**What it does**

This tool uses `MOSAIK`_ v. 2 to align reads to a reference sequence.

**License and citation**

This Galaxy tool is Copyright © 2005-2013 Pennsylvania State University, © 2013-2014 `CRS4 Srl.`_ and is released under the `Academic Free License v. 3.0`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _Academic Free License v. 3.0: http://opensource.org/licenses/AFL-3.0

You can use this tool only if you agree to the license terms of: `MOSAIK`_.

.. _MOSAIK: https://code.google.com/p/mosaik-aligner/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Lee2014|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Lee2014| replace:: Lee, W.-P., *et al.* (2014) MOSAIK: A Hash-Based Algorithm for Accurate Next-Generation Sequencing Short-Read Mapping. *PLoS ONE* 9(3), e90581
.. _Lee2014: http://www.plosone.org/article/info:doi/10.1371/journal.pone.0090581
  </help>
  <citations>
    <citation type="doi">10.1093/bioinformatics/btu135</citation>
    <citation type="doi">10.1371/journal.pone.0090581</citation>
  </citations>
</tool>
