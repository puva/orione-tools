<tool id="fastq_positional_quality_trimming" name="FASTQ positional and quality trimming" version="0.0.1">
  <description></description>
  <command interpreter="python">
    fastq_positional_quality_trimming.py -1 $input1 --maxlt $maxlengthtrim --lt $lefttrim --rt $righttrim --minqt $minqualtrim --avgqt $avgqualtrim --minlf $minlen --trimmed1 $trimmed1 --log $logfile
    #if $paired.paired_select == "paired"
      -2 $input2 --trimmed2 $trimmed2 --trimmedunpaired $trimmedunpaired
    #end if
  </command>

  <inputs>
    <conditional name="paired">
      <param name="paired_select" type="select" label="Is this library mate-paired?">
        <option value="single">Single-end</option>
        <option value="paired">Paired-end</option>
      </param>
      <when value="single">
        <param name="input1" type="data" format="fastqsanger" label="FASTQ file" help="FASTQ format with Sanger-scaled quality values (Galaxy fastqsanger datatype)" />
      </when>
      <when value="paired">
        <param name="input1" type="data" format="fastqsanger" label="Forward FASTQ file" help="FASTQ format with Sanger-scaled quality values (Galaxy fastqsanger datatype)" />
        <param name="input2" type="data" format="fastqsanger" label="Reverse FASTQ file" help="FASTQ format with Sanger-scaled quality values (Galaxy fastqsanger datatype)" />
      </when>
    </conditional>
    <param name="maxlengthtrim" type="integer" value="-1" label="Maximum length trimming" help="Trim reads longer then this value (useful for Ion Torrent); -1 for no trimming">
      <validator type="in_range" min="-1" />
    </param>
    <param name="lefttrim" type="integer" value="0" label="Left-side trimming" help="Number of bases to trim from 5' (left) end">
      <validator type="in_range" min="0" />
    </param>
    <param name="righttrim" type="integer" value="0" label="Right-side trimming" help="Number of bases to trim from 3' (right) end">
      <validator type="in_range" min="0" />
    </param>
    <param name="minqualtrim" type="integer" min="0" max="41" value="0" label="Minimum Phred quality score for right-side trimming" help="Starting from 3' (right) end, bases with quality less than this value will be trimmed" />
    <param name="avgqualtrim" type="float" min="0" max="41" value="0.0" label="Average Phred quality score for right-side trimming" help="Starting from 3' (right) end, bases will be trimmed one-by-one until the average read quality reaches this value" />
    <param name="minlen" type="integer" value="-1" label="Minimum length filtering" help="Reads shorter than given length will be discarded; -1 for no filtering">
      <validator type="in_range" min="-1" />
    </param>
  </inputs>
  <outputs>
    <data name="trimmed1" format="fastqsanger">
      <label>
        ${tool.name} on ${on_string}: trimmed
        #if $paired.paired_select == "paired"#
          forward
        #end if#
        FASTQ
      </label>
    </data>
    <data name="trimmed2" format="fastqsanger" label="${tool.name} on ${on_string}: trimmed reverse FASTQ">
      <filter>paired['paired_select'] == "paired"</filter>
    </data>
    <data name="trimmedunpaired" format="fastqsanger" label="${tool.name} on ${on_string}: trimmed unpaired reads">
      <filter>paired['paired_select'] == "paired"</filter>
    </data>
    <data name="logfile" format="txt" label="${tool.name} on ${on_string}: log" />
  </outputs>
  <tests>

  </tests>
  <help>
**What it does**

This tool trims paired-end FASTQ files on the basis of left/right position and/or quality score, retaining mate integrity.
Operations are executed in the order listed above.
Reads without mate after filtering are saved in a separate output file.

**Input formats**

This tool accepts files in Sanger FASTQ format (Galaxy *fastqsanger* datatype). Use the FASTQ Groomer tool to prepare your files.

**License and citation**

This Galaxy tool is Copyright © 2013-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

If you use this tool, please cite |Cuccuru2014|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
  </help>
</tool>
