# -*- coding: utf-8 -*-

import optparse

class PairedEndComposition(object):

    _MONOmer = ['A', 'C', 'G', 'T']

    _TWOmer = ['AA', 'AC', 'AG', 'AT', 'CA', 'CC', 'CG', 'CT', \
                'GA', 'GC', 'GG', 'GT', 'TA', 'TC', 'TG', 'TT']

    _THREEmer = ['AAA', 'ACA', 'AGA', 'ATA', 'CAA', 'CCA', 'CGA', 'CTA', \
                'GAA', 'GCA', 'GGA', 'GTA', 'TAA', 'TCA', 'TGA', 'TTA', \
                'AAC', 'ACC', 'AGC', 'ATC', 'CAC', 'CCC', 'CGC', 'CTC', \
                'GAC', 'GCC', 'GGC', 'GTC', 'TAC', 'TCC', 'TGC', 'TTC', \
                'AAG', 'ACG', 'AGG', 'ATG', 'CAG', 'CCG', 'CGG', 'CTG', \
                'GAG', 'GCG', 'GGG', 'GTG', 'TAG', 'TCG', 'TGG', 'TTG', \
                'AAT', 'ACT', 'AGT', 'ATT', 'CAT', 'CCT', 'CGT', 'CTT', \
                'GAT', 'GCT', 'GGT', 'GTT', 'TAT', 'TCT', 'TGT', 'TTT',]

    def __init__(self, leftfile, rightfile, maxfreqMono, maxfreqDime, maxfreqTrim, lefttrimmed, righttrimmed, unmatefile, discardedfile, logfile):
        self.leftfile = open(leftfile)
        self.rightfile = open(rightfile)
        self.logfile = logfile
        self.maxfreqMono = float(maxfreqMono)
        self.maxfreqDime = float(maxfreqDime)
        self.maxfreqTrim = float(maxfreqTrim)
        self.lefttrimmedfile = lefttrimmed
        self.righttrimmedfile = righttrimmed
        self.unmatefile = unmatefile
        self.discardedfile = discardedfile
        self.setoutL = []
        self.setoutR = []
        self.setoutU = []
        self.setoutD = []
        self.totalreads = 0
        self.matereads = 0
        self.unmatereads = 0
        self.discarded = 0
        try:
            while True:
                headL = self.leftfile.next().strip()
                headR = self.rightfile.next().strip()
                sequL = self.leftfile.next().strip()
                sequR = self.rightfile.next().strip()
                commL = self.leftfile.next().strip()
                commR = self.rightfile.next().strip()
                qualL = self.leftfile.next().strip()
                qualR = self.rightfile.next().strip()
                lenL = len(sequL)
                lenR = len(sequR)
                readL = headL + '\n' + sequL + '\n' + commL + '\n' + qualL
                readR = headR + '\n' + sequR + '\n' + commR + '\n' + qualR
                monoFreqL = []
                monoFreqR = []
                for monom in self._MONOmer:
                    monoFreqL.append(str(sequL.count(monom)))
                    monoFreqR.append(str(sequR.count(monom)))
                dimerFreqL = []
                dimerFreqR = []
                for dimer in self._TWOmer:
                    dimerFreqL.append(str(sequL.count(dimer)))
                    dimerFreqR.append(str(sequR.count(dimer)))
                trimerFreqL = []
                trimerFreqR = []
                for trimer in self._THREEmer:
                    trimerFreqL.append(str(sequL.count(trimer)))
                    trimerFreqR.append(str(sequR.count(trimer)))
                if lenL > 2 and lenR > 2:
                    self._check(lenL, lenR, monoFreqL, monoFreqR, dimerFreqL, dimerFreqR, trimerFreqL, trimerFreqR, readL, readR)
                self.totalreads += 1
        except StopIteration:
            self._writeDownMate()
            self._writeDownUnmate()
            self._writeDownDiscarded()
            self._setLog()

    def _check(self,  lenL, lenR, monoFreqL, monoFreqR, dimerFreqL, dimerFreqR, trimerFreqL, trimerFreqR, readL, readR):
        """ """
        freqMaxMonoL = round((float(max(monoFreqL))/float(lenL)*100), 1)
        freqMaxMonoR = round((float(max(monoFreqR))/float(lenR)*100), 1)
        freqMaxDimeL = round((float(max(dimerFreqL))/float(lenL -1)*100), 1)
        freqMaxDimeR = round((float(max(dimerFreqR))/float(lenR -1)*100), 1)
        freqMaxTrimL = round((float(max(trimerFreqL))/float(lenL -2)*100), 1)
        freqMaxTrimR = round((float(max(trimerFreqR))/float(lenR -2)*100), 1)
        if self._checkMonomers(freqMaxMonoL, freqMaxMonoR, readL, readR):
            if self._checkDimers(freqMaxDimeL, freqMaxDimeR, readL, readR):
                if self._checkTrimers(freqMaxTrimL, freqMaxTrimR, readL, readR):
                    self._setMateOut(readL, readR)

    def _checkMonomers(self, freqMaxMonoL, freqMaxMonoR, readL, readR):
        """ """
        if freqMaxMonoL <= self.maxfreqMono and freqMaxMonoR <= self.maxfreqMono:
            return True
        elif freqMaxMonoL <= self.maxfreqMono and freqMaxMonoR > self.maxfreqMono:
            self._setUnmateOut(readL)
            self._setDiscardedOut(readR)
            return False
        elif freqMaxMonoL > self.maxfreqMono and freqMaxMonoR <= self.maxfreqMono:
            self._setDiscardedOut(readL)
            self._setUnmateOut(readR)
            return False
        else:
            self._setDiscardedOut(readL)
            self._setDiscardedOut(readR)
            return False

    def _checkDimers(self, freqMaxDimeL, freqMaxDimeR, readL, readR):
        """ """
        if freqMaxDimeL <= self.maxfreqDime and freqMaxDimeR <= self.maxfreqDime:
            return True
        elif freqMaxDimeL <= self.maxfreqDime and freqMaxDimeR > self.maxfreqDime:
            self._setUnmateOut(readL)
            self._setDiscardedOut(readR)
            return False
        elif freqMaxDimeL > self.maxfreqDime and freqMaxDimeR <= self.maxfreqDime:
            self._setDiscardedOut(readL)
            self._setUnmateOut(readR)
            return False
        else:
            self._setDiscardedOut(readL)
            self._setDiscardedOut(readR)
            return False

    def _checkTrimers(self, freqMaxTrimL, freqMaxTrimR, readL, readR):
        """ """
        if freqMaxTrimL <= self.maxfreqTrim and freqMaxTrimR <= self.maxfreqTrim:
            return True
        elif freqMaxTrimL <= self.maxfreqTrim and freqMaxTrimR > self.maxfreqTrim:
            self._setUnmateOut(readL)
            self._setDiscardedOut(readR)
            return False
        elif freqMaxTrimL > self.maxfreqTrim and freqMaxTrimR <= self.maxfreqTrim:
            self._setDiscardedOut(readL)
            self._setUnmateOut(readR)
            return False
        else:
            self._setDiscardedOut(readL)
            self._setDiscardedOut(readR)
            return False

    def _setMateOut(self, readL, readR):
        """ """
        self.matereads += 1
        self.setoutL.append(readL)
        self.setoutR.append(readR)
        if len(self.setoutL) >= 500000:
            self._writeDownMate()

    def _setUnmateOut(self, read):
        """ """
        self.setoutU.append(read)
        if len(self.setoutU) >= 500000:
            self._writeDownUnmate()

    def _setDiscardedOut(self, read):
        """ """
        self.setoutD.append(read)
        if len(self.setoutD) >= 500000:
            self._writeDownDiscarded()

    def _writeDownMate(self):
        """ """
        with open(self.lefttrimmedfile, 'a') as outL:
            for element in self.setoutL:
                outL.write(element + '\n')
        self.setoutL = []

        with open(self.righttrimmedfile, 'a') as outR:
            for element in self.setoutR:
                outR.write(element + '\n')
        self.setoutR = []

    def _writeDownUnmate(self):
        """ """
        self.unmatereads += 1
        with open(self.unmatefile, 'a') as outU:
            for element in self.setoutU:
                outU.write(element + '\n')
        self.setoutU = []

    def _writeDownDiscarded(self):
        """ """
        self.discarded += 1
        with open(self.discardedfile, 'a') as outD:
            for element in self.setoutD:
                outD.write(element + '\n')
        self.setoutD = []

    def _setLog(self):
        """ """
        with open(self.logfile, 'w') as out:
            out.write('# Report of Paired-end compositional filtering\n')
            out.write('# Total paired reads     : ' + str(self.totalreads) + '\n')
            out.write('# Passing paired reads   : ' + str(self.matereads) + '\n')
            out.write('# Passing unpaired reads : ' + str(self.unmatereads) + '\n')
            out.write('# Discarded paired reads : ' + str(self.discarded) + '\n')


def __main__():
    parser = optparse.OptionParser()
    parser.add_option('--lf', dest='leftfile' , help='Forward reads')
    parser.add_option('--rf', dest='rightfile', help='Reverse reads')
    parser.add_option('--mm', dest='maxfreqMono', type='float', default=80, help='Maximum frequency monomers')
    parser.add_option('--md', dest='maxfreqDime', type='float', default=25, help='Maximum frequency dimers')
    parser.add_option('--mt', dest='maxfreqTrim', type='float', default=15, help='Maximum frequency trimers')
    parser.add_option('--lo', dest='leftrimmed', help='Output file for forward reads')
    parser.add_option('--ro', dest='righttrimmed', help='Output file for reverse reads')
    parser.add_option('--uo', dest='unmatefile', help='Output file for unpaired reads')
    parser.add_option('--do', dest='discardedfile', help='Output file for discarded reads')
    parser.add_option('--lg', dest='logfile', help='Logfile')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    PairedEndComposition(options.leftfile, options.rightfile, options.maxfreqMono, options.maxfreqDime, options.maxfreqTrim, options.leftrimmed, options.righttrimmed, options.unmatefile, options.discardedfile, options.logfile)


if __name__ == "__main__":
    __main__()
