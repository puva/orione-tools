SSPACE wrapper
==============

Configuration
-------------

sspace tool may be configured to use more than one CPU core by selecting an appropriate destination for this tool in Galaxy job_conf.xml file (see https://wiki.galaxyproject.org/Admin/Config/Jobs and https://wiki.galaxyproject.org/Admin/Config/Performance/Cluster ).

If you are using Galaxy release_2013.11.04 or later, this tool will automatically use the number of CPU cores allocated by the job runner according to the configuration of the destination selected for this tool.

If instead you are using an older Galaxy release, you should also add a line

  GALAXY_SLOTS=N; export GALAXY_SLOTS

(where N is the number of CPU cores allocated by the job runner for this tool) to the file

  <tool_dependencies_dir>/sspace/2.1/crs4/sspace/<hash_string>/env.sh

Version history
---------------

- Release 3: Fix missing requirement.
- Release 2: Change default for minimum contig length (-z) to 0. Update Orione citation. Update dependency to SSPACE Basic v2.1 . Add <citations>.
- Release 1: Directly call SSPACE, remove sspace.py and hidden library file output. Use $GALAXY_SLOTS instead of $SSPACE_SITE_OPTIONS. Add readme.rst .
- Release 0: Initial release in the Tool Shed.

Development
-----------

Development is hosted at https://bitbucket.org/crs4/orione-tools . Contributions and bug reports are very welcome!
