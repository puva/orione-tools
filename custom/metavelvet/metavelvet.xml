<tool id="metavelvet" name="MetaVelvet" version="0.1">
  <description>a short read assembler for metagenomics</description>
  <requirements>
    <requirement type="package" version="1.2.02">metavelvet</requirement>
  </requirements>
  <version_command>meta-velvetg 2&gt;&amp;1 | sed -n 's/.*Version = \([[:digit:].]\{1,\}\).*/\1/p'</version_command>
  <command>
    ln -s "${velveth_files.extra_files_path}/Roadmaps";
    ln -s "${velveth_files.extra_files_path}/Sequences";
    ln -s "$velvetg_file" Graph2;

    meta-velvetg .
    #if $discard_chimera
      -discard_chimera yes
    #else
      -discard_chimera no
    #end if
    #if str($max_chimera_rate)
      -max_chimera_rate $max_chimera_rate
    #end if
    #if str($repeat_cov_sd)
      -repeat_cov_sd $repeat_cov_sd
    #end if
    #if str($min_split_length)
      -min_split_length $min_split_length
    #end if
    #if str($valid_connections)
      -valid_connections $valid_connections
    #end if
    #if str($noise_connections)
      -noise_connections $noise_connections
    #end if
    #if $use_connections
      -use_connections yes
    #else
      -use_connections no
    #end if
    #if $report_split_detail
      -report_split_detail yes
    #else
      -report_split_detail no
    #end if
    #if $report_subgraph
      -report_subgraph yes
    #else
      -report_subgraph no
    #end if

    #if $exp_covs
      -exp_covs "$exp_covs"
    #end if
    #if str($min_peak_cov)
      -min_peak_cov $min_peak_cov
    #end if
    #if str($max_peak_cov)
      -max_peak_cov $max_peak_cov
    #end if
    #if str($histo_bin_width)
      -histo_bin_width $histo_bin_width
    #end if
    #if str($histo_sn_ratio)
      -histo_sn_ratio $histo_sn_ratio
    #end if

    #if str($cov_cutoff)
      -cov_cutoff $cov_cutoff
    #end if
    #if str($max_coverage)
      -max_coverage $max_coverage
    #end if
    #if str($long_cov_cutoff)
      -long_cov_cutoff $long_cov_cutoff
    #end if
    #if str($max_branch_length)
      -max_branch_length $max_branch_length
    #end if
    #if str($max_divergence)
      -max_divergence $max_divergence
    #end if
    #if str($max_gap_count)
      -max_gap_count $max_gap_count
    #end if
    #if str($min_contig_lgth)
      -min_contig_lgth $min_contig_lgth
    #end if

    #if $scaffolding
      -scaffolding yes
    #else
      -scaffolding no
    #end if
    #if str($exp_cov)
      -exp_cov $exp_cov
    #end if
    #if str($ins_length)
      -ins_length $ins_length
    #end if
    #if str($ins_length_sd)
      -ins_length_sd $ins_length_sd
    #end if
    #if str($ins_length2)
      -ins_length2 $ins_length2
    #end if
    #if str($ins_length2_sd)
      -ins_length2_sd $ins_length2_sd
    #end if
    #if str($ins_length_long)
      -ins_length_long $ins_length_long
    #end if
    #if str($ins_length_long_sd)
      -ins_length_long_sd $ins_length_long_sd
    #end if
    #if str($min_pair_count)
      -min_pair_count $min_pair_count
    #end if
    #if str($long_mult_cutoff)
      -long_mult_cutoff $long_mult_cutoff
    #end if
    #if $short_mate_paired
      -shortMatePaired yes
    #else
      -shortMatePaired no
    #end if

    #if $amos_file
      -amos_file yes
    #else
      -amos_file no
    #end if
    #if str($coverage_mask)
      -coverage_mask $coverage_mask
    #end if
    #if $unused_reads
      -unused_reads yes
    #else
      -unused_reads no
    #end if
    #if $alignments
      -alignments yes
    #else
      -alignments no
    #end if
    #if $export_filtered
      -exportFiltered yes
    #else
      -exportFiltered no
    #end if
    #if str($paired_exp_fraction)
      -paired_exp_fraction $paired_exp_fraction
    #end if
    >$output_log
  </command>

  <inputs>
<!-- MetaVelvet "input data" is a folder containing the output files from velveth (Sequences and Roadmaps) and velvetg (Graph2, using -exp_cov auto option)... -->
    <param name="velveth_files" type="data" format="velvet" label="Velvet dataset (Roadmaps + Sequences)" help="Prepared by velveth."/>
    <param name="velvetg_file" type="data" format="txt" label="Graph2" help="Graph2 file by velvetg."/>

<!-- Graph-splitting options (metagenome-specific) -->
    <param name="discard_chimera" type="boolean" checked="false" label="Discard chimera sub-graph (-discard_chimera)" help="" />
    <param name="max_chimera_rate" type="float" value="0.0" optional="true" label="Maximum allowable chimera rate (-max_chimera_rate)" help="" />
    <param name="repeat_cov_sd" type="float" value="0.1" optional="true" label="Standard deviation of repeat node coverages (-repeat_cov_sd)" help="" />
    <param name="min_split_length" type="integer" value="0" optional="true" label="Minimum node length required for repeat resolution (-min_split_length)" help="" />
    <param name="valid_connections" type="integer" value="1" optional="true" label="Minimum allowable number of consistent paired-end connections (-valid_connections)" help="" />
    <param name="noise_connections" type="integer" value="0" optional="true" label="Maximum allowable number of inconsistent paired-end connections (-noise_connections)" help="" />
    <param name="use_connections" type="boolean" checked="true" label="Use paired-end connections for graph splitting (-use_connections)" help="" />
    <param name="report_split_detail" type="boolean" checked="false" label="Report sequences around repeat nodes (-report_split_detail)" help="" />
    <param name="report_subgraph" type="boolean" checked="false" label="Report node sequences for each subgraph (-report_subgraph)" help="" />


<!-- Peak detection options (metagenome-specific) -->
    <param name="exp_covs" type="text" value="" optional="true" label="Expected coverages for each species in microbiome (-exp_covs)" help="Coverage values should be sorted in a descending order, e.g. “214_122_70_43_25_13.5“">
      <validator type="regex" message="Invalid expected coverage list">[\d.e\-_]+$</validator>
    </param>
    <param name="min_peak_cov" type="float" value="0.0" optional="true" label="Minimum peak coverage (-min_peak_cov)" help="" />
    <param name="max_peak_cov" type="float" value="500.0" optional="true" label="Maximum peak coverage (-max_peak_cov)" help="" />
    <param name="histo_bin_width" type="float" value="1.0" optional="true" label="Bin width of peak coverage histogram (-histo_bin_width)" help="" />
    <param name="histo_sn_ratio" type="float" value="10.0" optional="true" label="Signal-noise ratio to remove peak noises (-histo_sn_ratio)" help="" />

<!-- Contiging options (common to single-genome) -->
    <param name="cov_cutoff" type="float" value="" optional="true" label="Removal of low coverage nodes AFTER tour bus (-cov_cutoff)" help="Allow the system to infer it if left empty" />
    <param name="max_coverage" type="float" value="" optional="true" label="Removal of high coverage nodes AFTER tour bus (-max_coverage)" help="" />
    <param name="long_cov_cutoff" type="float" value="" optional="true" label="Removal of nodes with low long-read coverage AFTER tour bus (-long_cov_cutoff)" help="" />
    <param name="max_branch_length" type="integer" value="100" optional="true" label="Maximum length in base pair of bubble (-max_branch_length)" help="" />
    <param name="max_divergence" type="float" value="0.2" optional="true" label="Maximum divergence rate between two branches in a bubble (-max_divergence)" help="" />
    <param name="max_gap_count" type="integer" value="3" optional="true" label="Maximum number of gaps allowed in the alignment of the two branches of a bubble (-max_gap_count)" help="" />
    <param name="min_contig_lgth" type="integer" value="" optional="true" label="Minimum contig length exported to contigs.fa file (-min_contig_lgth)" help="Default value: hash length * 2" />

<!-- Scaffolding options (common to single-genome) -->
    <param name="scaffolding" type="boolean" checked="true" label="Scaffolding of contigs used paired end information (-scaffolding)" help="" />
    <param name="exp_cov" type="float" value="" optional="true" label="Expected coverage of unique regions (-exp_cov)" help="Allow the system to infer it if left empty" />
    <param name="ins_length" type="float" value="" optional="true" label="Expected distance between two paired end reads for the category (-ins_length)" help="" />
    <param name="ins_length_sd" type="float" value="" optional="true" label="Standard deviation of insert length for the category (-ins_length_sd)" help="Default value: insert length / 10" />
    <param name="ins_length2" type="float" value="" optional="true" label="Expected distance between two paired end reads for the category (-ins_length2)" help="" />
    <param name="ins_length2_sd" type="float" value="" optional="true" label="Standard deviation of insert length for the category (-ins_length2_sd)" help="Default value: insert length / 10" />
    <param name="ins_length_long" type="float" value="" optional="true" label="Expected distance between two long paired-end reads (-ins_length_long)" help="" />
    <param name="ins_length_long_sd" type="float" value="" optional="true" label="Standard deviation of insert length for the category (-ins_length_long_sd)" help="" />
    <param name="min_pair_count" type="integer" value="5" optional="true" label="Minimum number of paired end connections to justify the scaffolding of two long contigs (-min_pair_count)" help="" />
    <param name="long_mult_cutoff" type="integer" value="2" optional="true" label="Minimum number of long reads required to merge contigs (-long_mult_cutoff)" help="" />
    <param name="short_mate_paired" type="boolean" checked="false" label="For mate-pair libraries, indicate that the library might be contaminated with paired-end reads (-shortMatePaired)" help="" />

<!-- Output options (common to single-genome) -->
    <param name="amos_file" type="boolean" checked="false" label="Export assembly to AMOS file (-amos_file)" help="" />
    <param name="coverage_mask" type="integer" value="1" optional="true" label="Minimum coverage required for confident regions of contigs (-coverage_mask)" help="" />
    <param name="unused_reads" type="boolean" checked="false" label="Export unused reads in UnusedReads.fa file (-unused_reads)" help="" />
    <param name="alignments" type="boolean" checked="false" label="Export a summary of contig alignment to the reference sequences (-alignments)" help="" />
    <param name="export_filtered" type="boolean" checked="false" label="Export the long nodes which were eliminated by the coverage filters (-exportFiltered)" help="" />
    <param name="paired_exp_fraction" type="float" value="0.1" optional="true" label="Remove all the paired end connections which less than the specified fraction of the expected count (-paired_exp_fraction)" help="" />
  </inputs>

  <outputs>
    <data name="output_log" format="txt" label="${tool.name} on ${on_string}: log" />
    <data name="output_contigs" format="fasta" label="${tool.name} on ${on_string}: contigs" from_work_dir="meta-velvetg.contigs.fa" />
    <data name="output_last_graph" format="txt" label="${tool.name} on ${on_string}: LastGraph" from_work_dir="meta-velvetg.LastGraph" />
    <data name="output_last_graph_stats" format="tabular" label="${tool.name} on ${on_string}: LastGraph-stats" from_work_dir="meta-velvetg.LastGraph-stats.txt" />
    <data name="output_graph2_stats" format="tabular" label="${tool.name} on ${on_string}: Graph2-stats" from_work_dir="meta-velvetg.Graph2-stats.txt" />
    <data name="output_split_stats" format="tabular" label="${tool.name} on ${on_string}: split-stats" from_work_dir="meta-velvetg.split-stats.txt" />
    <data name="output_unused_reads" format="fasta" label="${tool.name} on ${on_string}: UnusedReads" from_work_dir="UnusedReads.fa" >
      <filter>unused_reads==True</filter>
    </data>
    <data name="output_low_coverage_contigs" format="fasta" label="${tool.name} on ${on_string}: lowCoverageContigs" from_work_dir="meta-velvetg.lowCoverageContigs.fa" >
      <filter>export_filtered==True</filter>
    </data>
    <data name="output_amos" format="txt" label="${tool.name} on ${on_string}: AMOS" from_work_dir="meta-velvetg.asm.afg" >
      <filter>amos_file==True</filter>
    </data>
  </outputs>

  <tests>
  </tests>
  <help>
**What it does**

MetaVelvet is a short read assembler for metagenomics.

**License and citation**

This Galaxy tool is Copyright © 2013-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `MetaVelvet`_.

.. _MetaVelvet: http://metavelvet.dna.bio.keio.ac.jp

If you use this tool, please cite:

- |Cuccuru2014|_
- |Namiki2012|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Namiki2012| replace:: Namiki T., *et al.* (2012) MetaVelvet: an extension of Velvet assembler to *de novo* metagenome assembly from short sequence reads. *Nucleic Acids Res.* 40(20), e155.
.. _Namiki2012: http://nar.oxfordjournals.org/content/40/20/e155
  </help>
</tool>
